

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const orderRoutes= require("./routes/order");
const productRoutes= require("./routes/product");
const userRoutes= require("./routes/user");
const cartRoutes= require("./routes/cart");

const app = express();


// connect to MONGODB:-----------------
mongoose.connect("MongoDB connection string must be put here",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);
mongoose.connection.once("open", ()=> console.log("Now connected to MongoDB Atlas"));
// -------------------------------------


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/orders", orderRoutes);
app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/cart", cartRoutes);

app.listen(process.env.PORT || 4001, ()=> {
	console.log(`API is now online on port ${process.env.PORT || 4001 }`)
});

