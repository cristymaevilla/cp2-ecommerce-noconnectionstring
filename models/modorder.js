const mongoose = require("mongoose");

const orderSchema= new mongoose.Schema({

	userId:{
		type:String,
		required: [true, "User Id method is required"]
	},
	productId:{
		type:String,
		default: "sample product id"
	},
	itemId:{
		type:String,
		default: "sample item id"
	},
	subTotal: {
		type:Number,
		required : [true, "Total amount is required"]
	},
	shippingFee: {
		type:Number,
		required : [true, "Total amount is required"]
	},
	totalAmount: {
		type:Number,
		required : [true, "Total amount is required"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},
	quantity:{
		type:Number,
		required : [true, "Total amount is required"]

	},

	paymentOption:{
		type:String,
		required: [true, "Payment option is required"]
	},
	isCancelled:{
		type:Boolean,
		default: false
	},

	status:{
		type:String,
		default: "pending"
	}

});


module.exports = mongoose.model("Order", orderSchema);
