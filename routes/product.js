const express = require("express");
const router = express.Router();

const productController= require("../controllers/product");
const auth= require("../auth");



// Create/Add product (ADMIN only):-------------------
router.post("/add", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	productController.addProduct(user, req.body).then(resultFromController => res.send(resultFromController));
});

// RETRIEVE ALL ACTIVE products:----------------------
router.get("/", (req,res)=>{
	productController.getAllActiveProducts().then(resultFromController => res.send(
		resultFromController));
})
// RETRIEVE by category:----------------------
router.get("/category", (req,res)=>{
	productController.getByCategory(req.body).then(resultFromController => res.send(
		resultFromController));
})
// RETRIEVE SINGLE product:---------------------------
router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController => res.send(
		resultFromController));
})

// UPDATE product information (ADMIN only):------------
router.put("/:productId/update", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.updateProduct(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})

// ARCHIVE product
router.put("/:productId/archive", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.archiveProduct(user,req.params).then(resultFromController => res.send(
		resultFromController));
})  

// ADD review by user
router.put("/:productId/addReview", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.addReview(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})


// DELETE review by user
router.put("/:productId/:reviewId/delete", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.deleteReview(user,req.params).then(resultFromController => res.send(
		resultFromController));
})




module.exports=router